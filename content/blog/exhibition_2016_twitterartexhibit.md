title:      	    #TwitterArtExhibit: NYC - 2016
date:       	    2016/03/30 20:00
category:	    Exhibition
categories:	    Exhibition
tags:       	    Group Exhibition
author:     	    Carolin Zöbelein
slug_dir:           Exhibitions
slug_subdir:        2016
slug:       	    TwitterArtExhibit
index_title:	    #TwitterArtExhibit: NYC - 2016
index_image:	    /images/exhibitions/2016/twitterartexhibit/Twitterartexhibit2016-Samdney-LowerQuality_Website.jpg
index_summary:	    2016/03/31 - 2016/04/21, Trygve Lie Gallery, New York, USA
sum_image:         	/images/exhibitions/2016/twitterartexhibit/Twitterartexhibit2016-Samdney-LowerQuality_Website.jpg
kind:               GroupExhibition
exhibition_date:    2016/03/31 - 2016/04/21
exhibition_place: 	Trygve Lie Gallery, 317 E 52nd St., New York, NY 10022
links:              Website, http://twitterartexhibit.org
links_info:         Website
description:        Twitter Art
sum_content:        Trygve Lie Gallery, 317 E 52nd St., New York, NY 10022<br /> <i>Artwork: Postcard (Acrylic paint) with title ’All is public - No secrets’</i>


<div class="two-columns-pages">
    <div class="col01-pages50">
        <img width="100%" src="/images/exhibitions/2016/twitterartexhibit/Twitterartexhibit2016-Samdney-LowerQuality_Website.jpg">
    </div>
    <div class="col02-pages50" style="padding-left: 5%;">
        <i><b>'All is public - No secrets'</b>, Postcard (Acrylic paint)</i><br /><br />
        If you are online, more and more parties try to get all of your information about you. It's just like if you would write it all on a postcard, which can be read by everybody.
    </div>
</div>
<div class="little-information">
    <i>Kewords: #twitterartexhibit 2016, @SamdneyTweet, Twitter, hacker, data, all, public, privat, secret, postcard, bash, John Doe, web, dark, dark web, encryption</i><br />
    <i>License: CC BY-ND 3.0 DE</i>
</div>






