title:      		    'Concept' - International Exhibition on Conceptual Art
date:       		    2016/05/05 20:00
category:		        Exhibition
categories:		        Exhibition
tags:       		    Group Exhibition
author:     		    Carolin Zöbelein
slug_dir:           	Exhibitions
slug_subdir:        	2016
slug:       			Concept
index_title:		    'Concept' - International Exhibition on Conceptual Art
index_image:            /images/exhibitions/2016/concept/IWasHere_Samdney.JPG
index_summary:		    2016/05/06 - 2016/05/29, CICA Museum, Korea
sum_image:			    /images/exhibitions/2016/concept/IWasHere_Samdney.JPG
kind:                   GroupExhibition
exhibition_date:	    2016/05/06 - 2016/05/29
exhibition_place:       CICA Museum, 196-30, Samdo-ro, Yangchon-eup, Gimpo-si, Gyeonggi-do, Korea 415-843
links:                  Website, https://cicamuseum.com/concept-2016-5-6-29, Website, https://cicamuseum.com/wp-content/uploads/2016/05/Concept_catalog.pdf
links_info:             Website, Catalog
description:            Conceptual Art
sum_content:		    CICA Museum, 196-30, Samdo-ro, Yangchon-eup, Gimpo-si, Gyeonggi-do, Korea 415-843<br /> <i>Artworks: Photo with title 'I was here', Photo with title 'Signing'</i>


<div class="two-columns-pages">
    <div class="col01-pages50">
        <img width="100%" src="/images/exhibitions/2016/concept/IWasHere_Samdney.JPG">
    </div>
    <div class="col02-pages50" style="padding-left: 5%;">
        <i><b>'I was here'</b>, Photo</i><br /><br />
        If you are using a cryptography system for communication and digital signing you have an unique fingerprint. In the analog life you are using your name to show 'I was here'. In the digital world you are using your fingerprint to identify yourself and your work.
    </div>
</div>
<div class="little-information">
    <i>Kewords: GnuPG, PGP, email, encryption, fingerprint, privacy, rsa, conceptual</i><br />
    <i>License: CC BY-SA 3.0 DE</i>
</div>

<br>

<div class="two-columns-pages">
    <div class="col01-pages50">
        <img width="100%" src="/images/exhibitions/2016/concept/Signing_Samdney.JPG">
    </div>
    <div class="col02-pages50" style="padding-left: 5%;">
        <i><b>'Signing'</b>, Photo</i><br /><br />
        GnuPG is a cryptography system for encrypted email communication which also includes the digital signing with an unique fingerprint for documents of every description. This fingerprint identifies your work and email address as the ones which belong to the right person.
    </div>
</div>
<div class="little-information">
    <i>Kewords: Signing, GnuPG, PGP, email, encryption, fingerprint, privacy, rsa, conceptual</i><br />
    <i>License: CC BY-SA 3.0 DE</i>
</div>
