title:      	    #Hashtag - A digital art short film series
date:       	    2019/08/10 20:00
category:	        Project
categories:	        Project
tags:       	    Running, Digital Art, Short Film, Series, Vimeo, Youtube
author:     		Carolin Zöbelein
slug_dir:			Projects
slug_subdir:		2019
slug:       	    Hashtag
index_title:	    #Hashtag
index_image:	    /images/projects/2019/hashtag/ThumbnailHashtagTeaserTrailer_website.jpg
index_summary:	    Since 2019/08/05, A digital art short film series
sum_image:          /images/projects/2019/hashtag/ThumbnailHashtagTeaserTrailer_website.jpg
kind:               Running
project_date:		Since 2019/08/05
project_place:   	Vimeo, Youtube
vimeo_trailer:		https://vimeo.com/352144538
youtube_trailer:	https://www.youtube.com/watch?v=K_anslmFTKA
description:        Digital Art

