Title:          About
Date:           2019/06/01 18:30
Slug:           about
cv:		<i>You can find my curriculum vitae as pdf version <a href="/files/cv_longversion.pdf" title="Curriculum Vitae" target="_blank">here</a>.</i>

<code><a href="/about.html#OnlinePresence">\#Online presence</a></code>
<code><a href="/about.html#CurriculumVitae">\#Curriculum Vitae</a></code>
<!-- <code><a href="/about.html#Mission">\#Mission</a></code> -->

<hr />
